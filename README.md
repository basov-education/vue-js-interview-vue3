# vue

This template should help get you started developing with Vue 3 in Vite.

## Recommended IDE Setup

[VSCode](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=Vue.volar) (and disable Vetur).

## Type Support for `.vue` Imports in TS

TypeScript cannot handle type information for `.vue` imports by default, so we replace the `tsc` CLI with `vue-tsc` for type checking. In editors, we need [Volar](https://marketplace.visualstudio.com/items?itemName=Vue.volar) to make the TypeScript language service aware of `.vue` types.

## Customize configuration

See [Vite Configuration Reference](https://vitejs.dev/config/).

## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Type-Check, Compile and Minify for Production

```sh
npm run build
```

### Lint with [ESLint](https://eslint.org/)

```sh
npm run lint
```

npm install
npm run format
npm run dev

# Тестовое задание на позицию frontend-vue-js-разработчик

## Тестовый проект полностью собран с использованием готовых функциональных решений и плагинов, таких как Vue-CLI и Vuetify

Необязательно пытаться выполнить задание, используя только Vuetify и CLI. Вы вольны использовать любые плагины и решения, и даже заменять уже готовый код.
Мы хотим понять, как Вы подходите к решению задач и пишете код, насколько хорошо разбираетесь в базовых принципах языка.
Будьте готовы к обсуждению решений на собеседовании.

```
Выполненное задание необходимо разместить в git и предоставить ссылку.
```

# Задание

1. Необходимо разделить содержимое компонента `ContentBlock` на отдельные компоненты для фильтров и списка пользователей (допускается использование своей вёрстки).
2. Добавить в структуру данных списка пользователей `users` недостающие для фильтрации параметры, и предусмотреть возможность его динамической загрузки из внешнего `.json API`
3. Реализовать фильтрацию универсально, используя _Vuex_ (или другое хранилище состояний) так, что бы состояние фильтров было доступно другим компонентам.
4. (_опционально_) Добавить визуальное обозначение процесса фильтрации (**preloader**).
5. Добавить в структуру данных пользователя информацию об адресе проживания и выводить её при клике на портрет.
6. (_опционально_) Сопроводить процесс фильтрации "интересным" визуальным переходом (`transitions`).

## Установка проекта

```
npm install
```

### Компиляция и hot-reloads для разработки

```
npm run serve
```
