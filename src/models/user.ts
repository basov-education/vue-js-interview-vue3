export default class User {
  avatar: string
  title: string
  subtitle: string
  score: number
  country: string
  address: string

  constructor() {
    this.avatar = ''
    this.title = ''
    this.subtitle = ''
    this.score = 0
    this.country = ''
    this.address = ''
  }
}
