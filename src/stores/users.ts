import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import User from '@/models/user'

export const useUsersStore = defineStore('users', () => {
  const users = ref<User[]>([])
  const isLoading = ref(false)

  const getUsers = computed(() => users.value)
  const getLoading = computed(() => isLoading.value)

  function loadUsers() {
    isLoading.value = true
    setTimeout(() => {
      users.value = [
        {
          avatar: 'https://cdn.vuetifyjs.com/images/lists/1.jpg',
          title: 'Brunch this weekend?',
          subtitle: `<span class="text--primary">Ali Connors</span> &mdash; I'll be in your neighborhood doing errands this weekend. Do you want to hang out?`,
          score: 15,
          country: 'russia',
          address: 'Адрес 1'
        },
        {
          avatar: 'https://cdn.vuetifyjs.com/images/lists/2.jpg',
          title: 'Summer BBQ <span class="grey--text text--lighten-1">4</span>',
          subtitle: `<span class="text--primary">to Alex, Scott, Jennifer</span> &mdash; Wish I could come, but I'm out of town this weekend.`,
          score: 25,
          country: 'usa',
          address: 'Адрес 2'
        },
        {
          avatar: 'https://cdn.vuetifyjs.com/images/lists/3.jpg',
          title: 'Oui oui',
          subtitle:
            '<span class="text--primary">Sandra Adams</span> &mdash; Do you have Paris recommendations? Have you ever been?',
          score: 35,
          country: 'usa',
          address: 'Адрес 3'
        },
        {
          avatar: 'https://cdn.vuetifyjs.com/images/lists/4.jpg',
          title: 'Birthday gift',
          subtitle:
            '<span class="text--primary">Trevor Hansen</span> &mdash; Have any ideas about what we should get Heidi for her birthday?',
          score: 10,
          country: 'russia',
          address: 'Адрес 4'
        },
        {
          avatar: 'https://cdn.vuetifyjs.com/images/lists/5.jpg',
          title: 'Recipe to try',
          subtitle:
            '<span class="text--primary">Britta Holt</span> &mdash; We should eat this: Grate, Squash, Corn, and tomatillo Tacos.',
          score: 3,
          country: 'usa',
          address: 'Адрес 5'
        }
      ]
      isLoading.value = false
    }, 1000)
  }

  function addUser(user: User) {
    users.value.push(user)
  }

  return { getUsers, getLoading, loadUsers, addUser }
})
